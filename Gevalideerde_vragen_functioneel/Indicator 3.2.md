---
title: 3.2. Had u één of meer contracten met zorgverzekeraars, zorgkantoren en/of DJI?
weight: 1
---
## Indicator

**Definitie:** n.v.t.

**Teller:** Niet van toepassing.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator is een placeholder voor vraag 3.2 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing op alle categorieën (klein, middelgroot, groot) zorgaanbieders.

Deze gegevens worden niet via KIK-V aangeleverd maar direct ingevoerd in het portaal DigiMV. Dit betreft onderstaande gegevens en vragen:

* Had u in het boekjaar één of meer contracten met zorgverzekeraars, zorgkantoren en/of Dienst Justitiële Inrichtingen (DJI)?

## Uitgangspunten

* De betekenis van deze gegevens is niet vastgelegd in de KIK-V modelgegevensset. De [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0) bevat voor deze vraag uitleg over de betekenis en wijze van invoeren.

## Berekening

Niet van toepassing
