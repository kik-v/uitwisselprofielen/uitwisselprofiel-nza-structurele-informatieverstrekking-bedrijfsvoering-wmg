---
title: 3.5. Diensten van andere rechtspersonen binnen een groepsstructuur.
weight: 1
---
### Indicator

**Definitie:** n.v.t.

**Teller:** Niet van toepassing.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator is een placeholder voor vraag 3.5 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing wanneer uit vraag 2 vragenlijst 2, van bijlage 4, van de Wijzigingsregeling Openbare jaarverantwoording WMG blijkt dat u onderdeel bent van een groep.

Deze gegevens worden niet via KIK-V aangeleverd maar direct ingevoerd in het portaal DigiMV. Dit betreft onderstaande gegevens en vragen:

* Maakt u ten behoeve van het verlenen van zorg gebruik van diensten van andere rechtspersonen en/of personenvennootschappen binnen de structuur?
* Wat is de reden hiervoor?

## Uitgangspunten

* De betekenis van deze gegevens is niet vastgelegd in de KIK-V modelgegevensset. De [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0) bevat voor deze vraag uitleg over de betekenis en wijze van invoeren.

## Berekening

Niet van toepassing
