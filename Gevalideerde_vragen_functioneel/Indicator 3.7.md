---
title: 3.7. Zorgopbrengsten per financieringsstroom
weight: 1
---
### Indicator

**Definitie:** n.v.t.

**Teller:** Niet van toepassing.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator is een placeholder voor vraag 3.7 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing voor zorgaanbieders die model D bijlage 1 en 2 van de Regeling openbare jaarverantwoording WMG hebben ingevuld.

Deze gegevens worden niet via KIK-V aangeleverd maar direct ingevoerd in het portaal DigiMV. Dit betreft onderstaande gegevens en vragen:

* Totale zorgopbrengsten uitgesplitst naar financieringsstroom:
  * Zorgverzekeringswet (Zvw)
  * Wet langdurige zorg (Wlz)
  * Forensische zorg
  * Overige zorg

## Uitgangspunten

* De betekenis van deze gegevens is niet vastgelegd in de KIK-V modelgegevensset. De [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0) bevat voor deze vraag uitleg over de betekenis en wijze van invoeren.

## Berekening

Niet van toepassing
