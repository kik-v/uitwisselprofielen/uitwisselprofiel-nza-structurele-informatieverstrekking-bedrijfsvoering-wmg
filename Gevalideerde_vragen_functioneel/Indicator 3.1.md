---
title: 3.1. Wat zijn de KvK nummers van hoofd- of onderaannemers?
weight: 1
---
## Indicator

**Definitie:** n.v.t.

**Teller:** Niet van toepassing.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator is een placeholder voor vraag 3.1 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing voor zorgaanbieders die bij vraag 3b van vragenlijst 2, in bijlage 4, van de Wijzigingsregeling Openbare jaarverantwoording WMG 'geheel of gedeeltelijk' hebben ingevuld.

Deze gegevens worden niet via KIK-V aangeleverd maar direct ingevoerd in het portaal DigiMV. Dit betreft onderstaande gegevens en vragen:

* Wat zijn de handelsregisternummers (KvK-nummers) van de onderaannemers waaraan u zorg hebt uitbesteed?
* Wat zijn de handelsregisternummers (KvK-nummers) van de hoofdaannemers voor wie u als onderaannemer zorg heeft verleend?

## Uitgangspunten

* De betekenis van deze gegevens is niet vastgelegd in de KIK-V modelgegevensset. De [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0) bevat voor deze vraag uitleg over de betekenis en wijze van invoeren.

## Berekening

Niet van toepassing
