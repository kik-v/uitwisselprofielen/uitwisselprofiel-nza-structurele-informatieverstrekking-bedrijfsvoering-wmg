---
title: 3.6. Gegevens verbonden partijen.
weight: 1
---
### Indicator

**Definitie:** n.v.t.

**Teller:** Niet van toepassing.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator is een placeholder voor vraag 3.6 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing voor zorgaanbieders in de categorieën middelgroot en groot.

Deze gegevens worden niet via KIK-V aangeleverd maar direct ingevoerd in het portaal DigiMV. Dit betreft onderstaande gegevens en vragen:

* Is er sprake van verbonden partijen?
* Wat zijn de handelsregisternummers (KVK-nummers) van de verbonden partijen?
* Hebben er in het boekjaar één of meerdere transacties met verbonden partijen plaatsgevonden onder niet-zakelijke condities?

## Uitgangspunten

* De betekenis van deze gegevens is niet vastgelegd in de KIK-V modelgegevensset. De [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0) bevat voor deze vraag uitleg over de betekenis en wijze van invoeren.

## Berekening

Niet van toepassing
