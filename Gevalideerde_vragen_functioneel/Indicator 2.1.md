---
title: 2.1. Aantal cliënten per wet 
weight: 1
---
## Indicator

**Definitie:** Aantal cliënten met één of meerdere indicaties in de meetperiode 1 januari t/m 31 december.

**Teller:** Aantal cliënten per wet.

**Noemer:** Niet van toepassing.

## Toelichting

De informatievraag betreft het aantal cliënten met één of meerdere indicaties per wet per verslagjaar. De indicator wordt berekend per wet over de meetperiode 1 januari t/m 31 december.

Deze indicator levert antwoord op vraag 2.1 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2023-11-25/0). Deze vraag is van toepassing op alle categorieën (klein, middelgroot, groot) zorgaanbieders.

## Uitgangspunten

* Een indicatie wordt geïncludeerd wanneer de datum of periode waarop deze betrekking heeft volledig of gedeeltelijk in de meetperiodeperiode ligt.
* Cliënten in onderaanneming worden meegeteld.
* Indicaties in de wetten Zvw, Wlz, Forensische zorg worden per wet gerapporteerd, alle overige indicaties vallen in de categorie "Overige zorg". 
* Het totaal van alle categorieën hoeft niet gelijk te zijn aan het totaal aantal unieke cliënten. Cliënten die in meerdere wetten een indicatie hadden tijdens meetperiode worden al die wetten meegeteld.
* Meetperiode is 1 januari v2024 t/m 31 december 2024.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle indicaties waarvan de periode waarop ze betrekking hebben volledig, of gedeeltelijk in de meetperiode valt.
2. Bepaal per indicatie de wet (Zvw, Wlz of Forensische zorg) waartoe deze behoort, en de cliënt die het betreft.
3. Bepaal op basis van stap 2 het aantal unieke cliënten per wet.
4. Bepaal op basis van stap 2 het aantal unieke cliënten die niet onder een wet (Zvw, Wlz of Forensische zorg) kunnen worden ingedeeld. Dit is het aantal cliënten overige zorg.

MeetPeriode: dd-mm-jjjj t/m dd-mm-jjjj

| Aantal cliënten Zvw | Aantal cliënten Wlz | Aantal cliënten Forensische zorg | Aantal cliënten Overige zorg |
| ------------------- | ------------------- | -------------------------------- | ---------------------------- |
|  Stap 3             | Stap 3              | Stap 3                           | Stap 4                       |
