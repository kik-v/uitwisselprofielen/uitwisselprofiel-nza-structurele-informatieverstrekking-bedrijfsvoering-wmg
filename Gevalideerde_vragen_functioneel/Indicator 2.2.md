---
title: 2.2. Aantal cliënten met leveringsvorm PGB per wet
weight: 1
---
## Indicator

**Definitie:** Aantal cliënten met een Wlz-indicatie en/of Zvw-traject met leveringsvorm PGB in de meetperiode 1 januari t/m 31 december.

**Teller:** Aantal cliënten met leveringsvorm PGB per wet.

**Noemer:** Niet van toepassing.

## Toelichting

De informatievraag betreft het aantal cliënten gedurende een meetperiode met leveringsvorm PGB per wet.

Deze indicator levert antwoord op vraag 2.2 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2023-11-25/0). Deze vraag is van toepassing op aanbieders wijkverpleging en aanbieders Wet langdurige zorg (Wlz).

## Uitgangspunten

* Cliënten met een Wlz-indicatie en/of Zvw-traject met leveringsvorm PGB op de peildatum worden geïncludeerd.
* Deze cliënten beschikken op enig moment in de meetperiode over het recht (ook wel legitimatie) om zorg te geleverd te krijgen.
* Meetperiode is 1 januari 2024 t/m 31 december 2024.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle cliënten die gedurende de meetperiode een indicatie hebben met leveringsvorm PGB.
2. Selecteer op basis van stap 1 alle indicaties die een Wlz-indicatie of een Zvw-indicatie betreffen.
3. Bepaal van alle indicaties welke cliënt het betreft.
4. Bepaal op basis van stap 1 het aantal unieke cliënten per wet.

Meetperiode: dd-mm-jjjj t/m dd-mm-jjjj

| Aantal cliënten met leveringsvorm PGB in de Zvw | Aantal cliënten met leveringsvorm PGB in de Wlz  |
|--------|--------|
| Stap 4 | Stap 4 |
