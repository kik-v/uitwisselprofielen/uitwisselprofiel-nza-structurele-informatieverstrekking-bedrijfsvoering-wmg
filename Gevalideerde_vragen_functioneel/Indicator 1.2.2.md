---
title: 1.2.2. Gemiddeld aantal ingezette fte personeel niet in loondienst (PNIL) met een zorgfunctie.
weight: 1
---

## Indicator

**Definitie:** Het gemiddeld aantal ingezette FTE personeel niet in loondienst (PNIL) met een zorgfunctie in de meetperiode 1 januari t/m 31 december.

**Teller:** Gemiddeld aantal ingezette FTE personeel niet in loondienst (PNIL) met een zorgfunctie in de meetperiode 1 januari t/m 31 december.

**Noemer:** Niet van toepassing.

## Toelichting

De indicator wordt berekend door per maand het aantal FTE zorgverleners in loondienst te bepalen op basis van de ingezette uren. Vervolgens wordt hiervan het gemiddelde bepaald en afgerond naar beneden naar een geheel getal.

Deze indicator levert een **gedeeltelijk** antwoord op vraag 1.2 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing op alle categorieën (klein, middelgroot, groot) zorgaanbieders.

Samen met de andere indicatoren 1.2.x. wordt de tabel bij vraag 1.2. als volgt gevuld:

|                                             | Antwoord          |
|---------------------------------------------|-------------------|
| Gem. aantal FTE zorgverleners in loondienst | Indicator 1.2.1.  |
| Gem. aantal FTE ingehuurde zorgverleners    | Indicator 1.2.2.  |

## Uitgangspunten

* Alle personeelsleden niet in loondienst met een zorgfunctie worden geïncludeerd.
* Personeelsleden niet in loondienst die tegelijkertijd over een of meerdere inhuur- of uitzendovereenkomsten beschikken, tellen mee in de berekening wanneer de ingezette uren geregistreerd zijn als zorgverlener (o.b.v. de functie in de inhuur- of uitzendovereenkomst).
* Het resultaat wordt gerapporteerd in de eenheid FTE36, m.a.w. één FTE is 36 uur per week.
* Voor de berekening wordt uitgegaan van 5 weken betaald verlof, dus 47 werkbare weken per jaar. 1 FTE36 (36 uur per week) komt dan neer op 36 uur/week * 47 weken/jaar = 1692 uur/jaar of 141 uur per maand.
* Meetperiode is 1 januari 2024 t/m 31 december 2024.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle inhuur- of uitzendovereenkomsten die gedurende januari van kracht zijn.
2. Als er minimaal één inhuur- of uitzendovereenkomst is dan telt deze maand mee in de berekening.
3. Selecteer op basis van stap 1 alle inhuur- of uitzendovereenkomsten die een zorgverlenerfunctie betreffen.
4. Bereken op basis van stap 3 voor iedere inhuur of uitzendovereenkomst het totaal aantal ingezette uren in de maand januari.
5. Bepaal op basis van stap 4 het totaal aantal ingezette uren (door PNIL met een zorgfunctie in januari).
6. Deel het resultaat van stap 5 door 141.
7. Herhaal de stappen 1 t/m 6 voor de overige maanden van het jaar.
8. Tel de resultaten van alle maanden bij elkaar op en deel dit door het aantal maanden dat meetelt (zie stap 2).
9. Rond het resultaat uit 8 naar beneden af naar een geheel getal.

Meetperiode: dd-mm-jjjj t/m dd-mm-jjjj

| Teller | Noemer            | Indicator (FTE36) |
|--------|-------------------|-------------------|
| Stap 9 |Niet van toepassing| Stap 9            |
