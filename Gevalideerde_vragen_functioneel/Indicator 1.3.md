---
title: 1.3. Personeelskosten uit jaarrekening
weight: 1
---
## Indicator

**Definitie:** n.v.t.

**Teller:** Niet van toepassing.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator is een placeholder voor vraag 1.3 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Afhankelijk van de jaarrekening modellen die bij de openbare jaarverantwoording zijn gebruikt bestaan twee scenario's.

Scenario 1:
Dit scenario is van toepassing voor zorgaanbieders die model D en model F bijlage 1 van de Wijzigingsregeling Openbare jaarverantwoording WMG hebben ingevuld en betreft onderstaande gegevens en vragen:

* Overige personeelskosten (in euro)
* Ingehuurd zorgpersoneel (in euro)
* Onderaannemers (in euro)
* Honorariumkosten vrijgevestigde medisch specialisten (in euro)

Scenario 2:
Dit scenario is van toepassing voor zorgaanbieders die model D en model F bijlage 2 van de Wijzigingsregeling Openbare jaarverantwoording WMG hebben ingevuld en betreft onderstaande gegevens en vragen:

* Overige personeelskosten (in euro)
* Ingehuurd zorgpersoneel (in euro)
* Onderaannemers (in euro)

## Uitgangspunten

* De betekenis van deze gegevens is niet vastgelegd in de KIK-V modelgegevensset. De [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0) bevat voor deze vraag uitleg over de betekenis en wijze van invoeren.

## Berekening

Niet van toepassing
