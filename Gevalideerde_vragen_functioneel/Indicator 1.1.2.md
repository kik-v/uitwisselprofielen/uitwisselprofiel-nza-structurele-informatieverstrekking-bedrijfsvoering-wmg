---
title: 1.1.2. Aantal contractuele FTE personeel in loondienst (PIL) zonder zorgfunctie op 1 januari
weight: 1
---

## Indicator

**Definitie:** Betreft het aantal contractuele uren van personeel in loondienst (PIL) zonder zorgfunctie, gedeeld door het aantal uren per week voor één FTE op 1 januari.

**Teller:** Aantal contractuele FTE personeel in loondienst (PIL) zonder zorgfunctie.

**Noemer:** Niet van toepassing.

## Omschrijving

De indicator betreft het aantal contractuele uren van personeel in loondienst (PIL) dat geen zorgfunctie heeft, gedeeld door het aantal uren per week voor één FTE op peildatum 1 januari.

Deze indicator levert een **gedeeltelijk** antwoord op vraag 1.1 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing op alle categorieën (klein, middelgroot, groot) zorgaanbieders.

Samen met de andere indicatoren 1.x. wordt de tabel bij vraag 1.1. als volgt gevuld:

|                                |Aantal FTE 1 jan    | Instroom aantal FTE   | Uitstroom aantal FTE   |
|--------------------------------|:------------------:|:---------------------:|:----------------------:|
| Zorgverleners in loondienst    | Indicator 1.1.1.   | Indicator 1.1.3.      | Indicator 1.1.5.       |
| Overig personeel in loondienst | Indicator 1.1.2.   | Indicator 1.1.4.      | Indicator 1.1.6.       |

## Uitgangspunten

* Peildatum is 1 januari 2024.
* Het resultaat wordt gerapporteerd in de eenheid FTE36, m.a.w. één FTE is 36 uur per week.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle arbeidsovereenkomsten die op 1 januari van kracht zijn.
2. Selecteer op basis van stap 1 alle arbeidsovereenkomsten die geen zorgfunctie betreffen.
3. Bepaal op basis van stap 2 van alle arbeidsovereenkomsten het aantal contractuele uren per week voor arbeidsovereenkomsten zonder zorgfunctie.
4. Bereken de indicator door de resultaten uit stap 3 bij elkaar op te tellen en te delen door het aantal uren per week voor één FTE.

Peildatum: dd-mm-jjjj

| Teller | Noemer            | Indicator (FTE36) |
|--------|-------------------|-------------------|
| Stap 4 |Niet van toepassing| Stap 4            |
