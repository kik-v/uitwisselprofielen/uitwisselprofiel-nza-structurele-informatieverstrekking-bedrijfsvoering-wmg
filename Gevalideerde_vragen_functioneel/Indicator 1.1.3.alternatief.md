---
title: 1.1.3. Aantal ingestroomde contractuele FTE personeel in loondienst (PIL) met een zorgfunctie
weight: 1
---

## Indicator

**Definitie:** Betreft het aantal contractuele uren van personeel in loondienst (PIL) met een zorgfunctie dat in de meetperiode van 1 januari t/m 31 december een arbeidsovereenkomst is aangegaan, gedeeld door het aantal uren per week voor één FTE.

**Teller:** Aantal contractuele FTE personeel in loondienst (PIL) met een zorgfunctie dat in de meetperiode van 1 januari t/m 31 december een arbeidsovereenkomst is aangegaan, gedeeld door het aantal uren per week voor één FTE.

**Noemer:** Niet van toepassing.

## Omschrijving

De indicator betreft het aantal contractuele uren van personeel in loondienst (PIL) met een zorgfunctie dat in de meetperiode van 1 januari t/m 31 december een arbeidsovereenkomst is aangegaan, gedeeld door het aantal uren per week voor één FTE.

Deze indicator levert een **gedeeltelijk** antwoord op vraag 1.1 uit de [regeling structurele informatieverstrekking bedrijfsvoering Wmg](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0). Deze vraag is van toepassing op alle categorieën (klein, middelgroot, groot) zorgaanbieders.

Samen met de andere indicatoren 1.1.x. wordt de tabel bij vraag 1.1. als volgt gevuld:

|                                |Aantal FTE 1 jan    | Instroom aantal FTE   | Uitstroom aantal FTE   |
|--------------------------------|:------------------:|:---------------------:|:----------------------:|
| Zorgverleners in loondienst    | Indicator 1.1.1.   | Indicator 1.1.3.      | Indicator 1.1.5.       |
| Overig personeel in loondienst | Indicator 1.1.2.   | Indicator 1.1.4.      | Indicator 1.1.6.       |

## Uitgangspunten

* Alleen arbeidsovereenkomsten die een zorgfunctie betreffen tellen mee.
* Het aantal contractuele uren van een medewerker op een dag is het totaal van alle contractuele uren uit alle (gelijktijdige) arbeidsovereenkomsten (die een zorgfunctie betreffen) op die dag.
* Steeds wordt per medewerker bezien welke mutaties er zijn in het aantal contractuele uren. Als het aantal contractuele uren tussen twee dagen in de meetperiode is toegenomen dan is de instroom op die dag, voor de betreffende medewerker, gelijk aan die toename. Als er een afname is in het aantal contractuele uren tussen twee dagen in de meetperiode dan is de uitstroom op die dag, voor de betreffende medewerker, gelijk aan die afname.
* Voorbeeld/verduidelijking: In het uitzonderlijke geval dat een medewerker op maandag z'n laatste werkdag heeft volgens contract 1, en vervolgens op woensdag start met contract 2. Dan tellen maandag alle oude contractuele uren mee als uitstroom, en woensdag alle nieuwe contractuele uren als instrooom. Als voor dezelfde medewerker het nieuwe contract op dinsdag ingaat, dan telt bij een verhoging van uren het verschil als instroom, bij een verlaging het verschil als uitstroom.
* Het resultaat wordt gerapporteerd in de eenheid FTE36, m.a.w. één FTE is 36 uur per week.
* De meetperiode is 1 januari 2024 t/m 31 december 2024.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle arbeidsovereenkomsten die een zorgfunctie betreffen.
2. Selecteer op basis van stap 1 alle arbeidsovereenkomsten met een startdatum in de meetperiode.
3. Selecteer op basis van stap 1 alle aanpassingen in arbeidsovereenkomsten met een mutatiedatum in de meetperiode waardoor het aantal contractuele uren is toegenomen.
4. Bepaal voor alle arbeidsovereenkomsten uit stap 2 en aanpassingen in arbeidsovereenkomsten uit stap 3 het aantal uren instroom. Dit is gelijk aan: (het totaal aantal contractuele uren op de startdatum c.q. mutatiedatum) - (het totaal aantal contractuele uren op de dag voor de startdatum c.q. mutatiedatum). Als de uitkomst van dit verschil negatief is, dan wordt de instroom op 0 gesteld.
    * Als op één datum meerdere arbeidsovereenkomsten starten of gemuteerd zijn, dan dient voorgaande berekening voor dat cluster slechts eenmaal uitgevoerd te worden.
5. Bereken de indicator door de resultaten uit stap 3 bij elkaar op te tellen en te delen door het aantal uren per week voor één FTE.

Meetperiode: dd-mm-jjjj t/m dd-mm-jjjj

| Teller | Noemer            | Indicator (FTE36) |
|--------|-------------------|-------------------|
| Stap 4 |Niet van toepassing| Stap 4            |
