---
title: Algemeen
weight: 2
---

**Uitwisselprofiel NZa - Aanvullende vragen DigiMV**
In de Wet marktordening gezondheidszorg (Wmg) staat beschreven dat de zorgaanbieders en zorgverzekeraars informatie moeten verschaffen over hun aanbod, tarieven, kwaliteit en andere eigenschappen van aangeboden zorg. Er staat ook beschreven hoe de tarieven in de zorg tot stand komen en de wet maakt het mogelijk om in de gaten te houden of de zorgmarkten (zorgverzekering, zorginkoop én zorgverlening) goed werken en om in te grijpen als dat niet zo is.

Gelet op artikel 62 van de Wet marktordening gezondheidszorg (Wmg), kan de Nederlandse Zorgautoriteit (NZa) regels stellen inhoudende welke gegevens en inlichtingen regelmatig moeten worden verstrekt dan wel onder welke omstandigheden deze moeten worden verstrekt door zorgaanbieders.

Gelet op artikel 68 van de Wet marktordening gezondheidszorg (Wmg), kan de NZa regels stellen inhoudende aan wie daarbij te bepalen gegevens en inlichtingen als bedoeld in artikel 62, moeten worden verstrekt, het tijdstip en de wijze waarop en de vorm waarin de gegevens en inlichtingen moeten worden verstrekt, of door wie en de wijze waarop de gegevens moeten worden bewerkt, of door wie en de wijze waarop de gegevens dan wel de bewerkingen van die gegevens moeten worden bekendgemaakt.

Dit uitwisselprofiel gaat over de informatie die de NZa uitvraagt binnen de Regeling structurele informatieverstrekking bedrijfsvoering Wmg.

**Doel van de uitvraag**

De [wetten.nl - Regeling - Regeling structurele informatieverstrekking bedrijfsvoering Wmg - BWBR0046300](https://wetten.overheid.nl/BWBR0046300/2025-01-10/0) schrijft voor welke gegevens en inlichtingen zorgaanbieders jaarlijks moeten verstrekken met betrekking tot hun bedrijfsvoering, als ook onder welke omstandigheden gegevens en inlichtingen moeten worden verstrekt en aan wie, het tijdstip waarop deze moeten worden verstrekt, en in welke vorm.

**Scope van de uitvraag**

De scope van de uitvraag omvat de bijlage van de Regeling Structurele informatieverstrekking bedrijfsvoering Wmg (zie de link Regeling hierboven voor de bijlage). Deze bijlage bevat 12 vragen. Het gaat om informatie die naar de aard niet openbaar mag worden en bestemd is voor de toezichthouders in de zorg (IGJ en NZa). Deze gegevens worden via een afgeschermde, niet openbare vragenlijst uitgevraagd. Alleen de IGJ en NZa ontvangen deze informatie. Deze informatie is dan ook bestemd voor de uitoefening van hun taken.
De NZa gebruikt voor de uitoefening van haar taken ook een deel van de gegevens die uitgevraagd worden voor het openbare gedeelte/ de jaarverantwoording middels DigiMV. Deze zitten niet in de scope van dit uitwisselprofiel. Wel wordt zo veel mogelijk in kaart gebracht om welke gegevens het gaat en hoe die zich verhouden tot de (12 aanvullende) vragen.

