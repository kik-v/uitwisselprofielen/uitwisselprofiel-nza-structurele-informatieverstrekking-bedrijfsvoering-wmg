---
title: Technische interoperabiliteit
weight: 4
---
## Technische interoperabiliteit
Uitwisseling loopt in eerste instantie nog via de huidige manier, via het DigiMV portaal, het portaal waar nu ook gebruikt van kan worden gemaakt. Het datastation kan voor een aantal van de indicatoren wel al gebruikt worden om de berekeningen geautomatiseerd te laten plaatsvinden. Het antwoord wordt vervolgens handmatig ingevoerd in DigiMV. Om welke indicatoren het gaat staat hierboven beschreven in de tabel.