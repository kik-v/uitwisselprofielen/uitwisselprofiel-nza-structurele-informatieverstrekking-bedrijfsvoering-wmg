---
title: Organisatorische interoperabiliteit
weight: 2
---
## Doelgroepen
Voor kleine organisaties geldt een kleinere uitvraag dan voor grote organisaties. Voor eenmanszaken in het bijzonder wordt geregeld dat zij een zestal ratio’s openbaar maken over de bedrijfsvoering en een vragenlijst beantwoorden, in tegenstelling tot andere organisaties die bijvoorbeeld een controleverklaring en jaarrekening openbaar maken.

## Gewenste momenten van aanlevering
Deze 14 aanvullende vragen zijn als bijlage opgenomen bij de uitvraag Jaarverantwoording. De jaarverantwoording moet jaarlijks vóór 1 juni openbaar worden gemaakt over het boekjaar ervoor. Dus uiterlijk 31 mei 2025 wordt de jaarverantwoording over boekjaar 2024 openbaar gemaakt.
Zorgaanbieders die een jaarrekening opstellen op grond van de Provinciewet of Gemeentewet maken de jaarverantwoording uiterlijk 15 juli openbaar.

## Gewenste moment van terugkoppeling
NTB

## Looptijd
De looptijd van het uitwisselprofiel is continu doorlopend tot het moment van wijziging. Op dit moment wordt de uitvraag rondom de jaarverantwoording en de aanvullende vragen jaarlijks gewijzigd en is de looptijd dus in principe een jaar.

## Gewenste bewaartermijn
NTB

## Afspraken bij vragen over hetgeen wordt uitgevraagd 
NTB

## Afspraken bij twijfels over de kwaliteit van gegevens
NTB

## In- en exclusiecriteria
Alle zorgaanbieders en combinatie-instellingen die een verantwoordingsplicht hebben volgens de Wtza.
