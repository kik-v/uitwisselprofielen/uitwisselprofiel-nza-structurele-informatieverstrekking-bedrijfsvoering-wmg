---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag
Dit uitwisselprofiel kent zijn grondslag in de volgende wetsartikelen:

* Wet marktordening gezondheidszorg: artikel 60,61,62,63,65,68 en 70
* Zorgverzekeringswet: artikel 88 en 89
* [Regeling structurele informatieverstrekking bedrijfsvoering Wmg - TH/NR-025](https://puc.overheid.nl/nza/doc/PUC_700571_22/)

Lees hiervoor ook de aanvullende toelichting in het juridisch kader, te vinden binnen de Afsprakenset via het [Juridisch kader](https://kik-v-publicatieplatform.nl/afsprakenset/_/Documentatie/juridisch-kader)
In het kader van dit uitwisselprofiel worden geen persoonsgegevens uitgewisseld. De juridische grondslag benodigd voor het uitwisselen van persoonsgegevens is hier dus niet opgenomen.
