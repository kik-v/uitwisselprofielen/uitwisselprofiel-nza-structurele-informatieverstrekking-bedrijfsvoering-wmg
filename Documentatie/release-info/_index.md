---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.0.0-RC1 |
| Versie | CONCEPT 0.9 |
| Doel | Release versie 1.0 bevat de informatie die de Nederlandse Zorgautoriteit (NZa) nodig heeft in het kader van de Regeling structurele informatieverstrekking bedrijfsvoering Wmg.|
| Doelgroep | Nederlandse Zorgautoriteit; Zorgaanbieders verpleeghuiszorg |
| Totstandkoming | De ontwikkeling van release 1.0 is uitgevoerd door het programma KIK-V in samenwerking met de NZa. Release 1.0 wordt vastgesteld door de Ketenraad KIK-V op basis van versie 0.9 |

